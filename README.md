# Qui nous sommes

***Groupe P15***

| NOM    | PRENOM   |
|--------|----------|
| IDIR   | Marc     |
| KANDIL | Mohamed  |
| LEGAY  | Valentin |
| PIAU   | Corentin |
 
===

# Notre travail

## Question 1

### Création de l'environnement

_Éxécuter les commandes suivantes :_

```shell
docker-compose build

docker-compose up -d

docker exec -ti CC15 bash

# Éxécuter cette commande dans le terminal du container, ouvert par la commande précédente.
symfony new cc15projet --webapp
```

### Lancer le server

_Éxecuter les commandes suivantes dans le terminal du container :_

```shell
cd cc15projet

symfony server:start --no-tls --d
```

### Installation de `webpack encore`

_Éxécuter les commandes suivantes dans le terminal, ouvert par la commande
vu dans la section [création](#création-de-lenvironnement)_

```shell
symfony composer require symfony/webpack-encore-bundle

npm install

npm install bootstrap

npm install bootstrap-icons

npm run dev  # après avoir fais les modif dans les fichiers nécessaires
```

### installation de bootstrap(5.3.0-alpha1)

_Éxécuter la commande suivantes dans le terminal, ouvert par la commande
vu dans la section [création](#création-de-lenvironnement)_

```shell
npm install bootstrap@5.3.0-alpha1
```

## Question 2

### Création de la BDD

```shell
symfony console doctrine:database:create
```

### Création de l'entité Atelier

```shell
symfony console make:entity Atelier
```

### Création du fichier de migration

```shell
symfony console make:migration
```

### Création de la table 

```shell
symfony console doctrine:migrations:migrate
```

## Question 3

### Installation de faker

```shell
symfony composer require fakerphp/faker
```

### Installation de fixtures

```shell
symfony composer require orm-fixtures --dev
```

### Création d'une fixture

```shell
symfony console make:fixture
```

### Exécuter la fixture

```shell
symfony console doctrine:fixtures:load
```

## Question 4

### Création d'un crud
```shell
symfony console make:crud Atelier
```

## Question 5
> Ajout d'une navbar


## Question 6
```shell
symfony composer require cebe/markdown
```

## Question 7

```shell
symfony console make:user
symfony console make:auth
symfony console make:entity user
symfony console make:migration
symfony console doctrine:migrations:migrate
symfony console make:registration-form
symfony console doctrine:fixtures:load
```

## Question 8

```shell
symfony console make:entity Atelier
symfony console make:migration
symfony console doctrine:migrations:m
symfony  console doctrine:fixtures:load
```

## Question 9

> modification de la sécurité sur la création d'atelier
> 
> ajout automatique de l'utilisateur instructeur connecté à la création d'un atelier.

## Question 10

> modification de la sécurité sur la classe atelier et de son affichage
> 
> redirection automatique sur la page atelier si l'on est déja connecté et que l'on veut s'inscrire ou se connecter

## Question 11

> Bootstrapification
> Rework de la navbar

## Question 12

> Ajout des routes d'inscription et désinscription
> 
> Définition du rôle par défaut comme apprenti
> 
> Ajout d'un bouton dynamique d'inscription/désincription pour les apprentis
 
## Question 13

> Ajout des routes `/atelier/inscriptions` et `/atelier/inscrits/<id atelier>`
> 
> Ajout des pages correspondant à ces routes

## Question 14

> Amélioration de la navbar
> 
> Changement du thème graphique pour des teintes plus neutres

## Question 15

> Implémentation pour un ADMIN d'ajouter le rôle INSTRUCTEUR à un utilisateur ainsi que d'ajouter le rôle ADMIN a un INSTRUCTEUR.
> 
> Implémentation pour un ADMIN de supprimer le rôle INSTRUCTEUR d'un INSTRUCTEUR

## Question 16

> Évolution des controleurs pour une solution viable.
> 
> Adaptation des contraintes de sécurité aux nouveaux URL

> En dépit d'avoir envisagé la création de contrôleurs distincts,
> nous avons voulu tester cette idée et avons été satisfait du résultat.

## Question 17

> Ajout d'un entité Note

```shell
symfony console doctrine:migrations:migrate
```

> Ajout de l'action sur la page d'inscriptions
> 
> Script JS de notation des ateliers

## Question 18

> Ajout d'un aperçu des notes moyennes.
> 
> Fix de bugs mineurs (notamment la modification
> de ses propres rôles en tant qu'admin).
> 
> Suppression du bouton "Ateliers" de la barre de navigation.