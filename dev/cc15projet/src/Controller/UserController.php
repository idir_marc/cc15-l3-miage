<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\UserType;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/admin')]
class UserController extends AbstractController
{
    #[Route('/', name: 'app_user_index', methods: ['GET'])]
    public function index(UserRepository $userRepository): Response
    {
        if (!$this->isGranted('ROLE_ADMIN')) {
            return $this->redirectToRoute('app_atelier_index');
        }
        return $this->render('user/index.html.twig', [
            'users' => $userRepository->findAll(),
            'user' => $this->getUser(),
        ]);
    }

    #[Route('/{id}', name: 'app_user_show', methods: ['GET'])]
    public function show(User $user): Response
    {
        return $this->render('user/show.html.twig', [
            'user' => $user,
        ]);
    }

    #[Route('/{id}', name: 'app_user_delete', methods: ['POST'])]
    public function delete(Request $request, User $user, UserRepository $userRepository): Response
    {
        if (!$this->isGranted('ROLE_ADMIN')) {
            return $this->redirectToRoute('app_atelier_index');
        }
        if ($this->isCsrfTokenValid('delete'.$user->getId(), $request->request->get('_token')) && !$user == $this->getUser()) {
            if (!in_array("ROLE_ADMIN",$user->getRoles())) {
                $userRepository->remove($user, true);
            }
        }

        return $this->redirectToRoute('app_user_index', [], Response::HTTP_SEE_OTHER);
    }

    #[Route('/giveadmin/{id}', name: 'app_user_give_admin', methods: ['GET','POST'])]
    public function giveAdmin(Request $request, User $user, UserRepository $userRepository, EntityManagerInterface $manager): Response
    {
        $i = '0';

        if (in_array("ROLE_ADMIN",$this->getUser()->getRoles()) && !$user == $this->getUser()){
            if (in_array("ROLE_INSTRUCTEUR",$user->getRoles())){
                $user->addRole("ROLE_ADMIN");
                $manager->persist($user);
                $manager->flush();
                $i = '1';
            }
        }

        return new Response($i);
        //return $this->redirectToRoute('app_user_index', [], Response::HTTP_SEE_OTHER);
    }

    #[Route('/giveinstructeur/{id}', name: 'app_user_give_instructeur', methods: ['GET','POST'])]
    public function giveInstructeur(Request $request, User $user, UserRepository $userRepository, EntityManagerInterface $manager): Response
    {
        $i = '0';

        if (in_array("ROLE_ADMIN",$this->getUser()->getRoles()) && !$user == $this->getUser()){
            if (!in_array("ROLE_INSTRUCTEUR",$user->getRoles())){
                $user->addRole("ROLE_INSTRUCTEUR");
                $manager->persist($user);
                $manager->flush();
                $i = '1';
            }
        }

        return new Response($i);
        //return $this->redirectToRoute('app_user_index', [], Response::HTTP_SEE_OTHER);
    }

    #[Route('/removeinstructeur/{id}', name: 'app_user_remove_instructeur', methods: ['GET','POST'])]
    public function removeInstructeur(User $user, UserRepository $userRepository, EntityManagerInterface $manager): Response
    {
        $i = '0';

        if (in_array("ROLE_ADMIN",$this->getUser()->getRoles()) && !$user == $this->getUser()){
            if (in_array("ROLE_INSTRUCTEUR",$user->getRoles())){
                $user->removeRole("ROLE_INSTRUCTEUR");
                $manager->persist($user);
                $manager->flush();
                $i = '1';
            }
        }

        return new Response($i);
        //return $this->redirectToRoute('app_user_index', [], Response::HTTP_SEE_OTHER);
    }

}
