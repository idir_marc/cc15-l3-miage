<?php

namespace App\Controller;

use App\Entity\Atelier;
use App\Entity\Note;
use App\Form\AtelierType;
use App\Repository\AtelierRepository;
use App\Repository\NoteRepository;
use App\Repository\UserRepository;
use cebe\markdown\Markdown;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/')]
class AtelierController extends AbstractController
{
    #[Route('/', name: 'app_atelier_index', methods: ['GET'])]
    public function index(AtelierRepository $atelierRepository, Markdown $markdown): Response
    {
        $ateliers = $atelierRepository->findAll();
        $parsedAteliers = [];
        $notes = [];
        foreach ($ateliers as $atelier){
            $parseAtelier = $atelier;
            $parseAtelier->setDescription($markdown->parse($atelier->getDescription()));
            $parsedAteliers[] = $parseAtelier;
            $notes[$atelier->getId()] = $atelier->avgNotes();
        }

        return $this->render('atelier/index.html.twig', [
            'ateliers' => $parsedAteliers,
            'notes' => $notes,
        ]);
    }

    #[Route('/manage/new', name: 'app_atelier_new', methods: ['GET', 'POST'])]
    public function new(Request $request, AtelierRepository $atelierRepository): Response
    {

        $atelier = new Atelier();
        $atelier->setInstructeur($this->getUser());
        $form = $this->createForm(AtelierType::class, $atelier);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $atelierRepository->save($atelier, true);

            return $this->redirectToRoute('app_atelier_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('atelier/new.html.twig', [
            'atelier' => $atelier,
            'form' => $form,
        ]);
    }

    #[Route('/show/{id}', name: 'app_atelier_show', methods: ['GET'])]
    public function show(Atelier $atelier, Markdown $markdown): Response
    {
        $atelierParse = $atelier;
        $atelierParse->setDescription($markdown->parse($atelier->getDescription()));
        $inscrit = false;
        if ($this->isGranted('IS_AUTHENTICATED_FULLY') ||
            $this->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            $inscrit = $atelier->getListeapprentis()->contains($this->getUser());
        }
        return $this->render('atelier/show.html.twig', [
            'atelier' => $atelierParse,
            'inscrit' => $inscrit,
            'note' => $atelier->avgNotes(),
        ]);
    }

    #[Route('/manage/edit/{id}', name: 'app_atelier_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, Atelier $atelier, AtelierRepository $atelierRepository): Response
    {
        $form = $this->createForm(AtelierType::class, $atelier);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $atelierRepository->save($atelier, true);

            return $this->redirectToRoute('app_atelier_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('atelier/edit.html.twig', [
            'atelier' => $atelier,
            'form' => $form,
        ]);
    }

    #[Route('/manage/delete/{id}', name: 'app_atelier_delete', methods: ['POST'])]
    public function delete(Request $request, Atelier $atelier, AtelierRepository $atelierRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$atelier->getId(), $request->request->get('_token'))) {
            $atelierRepository->remove($atelier, true);
        }

        return $this->redirectToRoute('app_atelier_index', [], Response::HTTP_SEE_OTHER);
    }

    #[Route('/inscription/{id}', name:'app_atelier_inscription', methods: ['GET', 'POST'])]
    public function subscribe(Atelier $atelier, UserRepository $userRepository, EntityManagerInterface $manager): Response
    {
        $user = $userRepository->findOneBy(['email' => $this->getUser()->getUserIdentifier()]);

        if ($user !== null) {
            $atelier->addListeapprenti($user);
            //$user->addListeAtelier($atelier);
            //$atelierRepository->save($atelier);

            $manager->persist($atelier);
            $manager->flush();
        }

        return $this->redirectToRoute('app_atelier_show', [
            'id' => $atelier->getId(),
        ], Response::HTTP_SEE_OTHER);
    }

    #[Route('/desinscription/{id}', name:'app_atelier_desinscription', methods: ['GET', 'POST'])]
    public function unsubscribe(Request $request, Atelier $atelier, UserRepository $userRepository,
                                EntityManagerInterface $manager): Response
    {
        $user = $userRepository->findOneBy(['email' => $this->getUser()->getUserIdentifier()]);

        if ($user !== null) {
            $atelier->removeListeApprenti($user);
            //$user->addListeAtelier($atelier);
            //$atelierRepository->save($atelier);

            $manager->persist($atelier);
            $manager->flush();
        }

        return $this->redirectToRoute('app_atelier_show', [
            'id' => $atelier->getId(),
        ], Response::HTTP_SEE_OTHER);
    }

    #[Route('/manage/inscrits/{id}', name: 'app_atelier_liste_users', methods: ['GET'])]
    public function displayListUser(Atelier $atelier): Response
    {
        $users = $atelier->getListeapprentis();


        return $this->render('atelier/listeinscrits.html.twig', [
            'users' => $users,
            'atelier' => $atelier
        ]);
    }


    #[Route('/inscriptions', name:'app_atelier_inscriptions', methods: ['GET','POST'])]
    public function listInscriptions(UserRepository $userRepository, Markdown $markdown): Response
    {
        if ($this->getUser() === null) {
            return $this->redirectToRoute('app_login');
        }

        $user = $userRepository->findOneBy(['email' => $this->getUser()->getUserIdentifier()]);

        $parsedAteliers = [];
        $ateliersNotes = [];
        $avgNotes = [];

        foreach ($user->getListeAteliers() as $atelier) {
            $parse = $atelier;
            $parse->setDescription($markdown->parse($atelier->getDescription()));
            $parsedAteliers[] = $atelier;
            $avgNotes[$atelier->getId()] = $atelier->avgNotes();
        }
        foreach ($user->getNotes() as $note) {
            $ateliersNotes[$note->getAtelier()->getId()] = $note->getValue();
        }

        return $this->render('atelier/inscriptions.html.twig', [
            'ateliers' => $parsedAteliers,
            'notes' => $ateliersNotes,
            'avgNotes' => $avgNotes,
        ]);
    }

    #[Route('/noter/{id}/{value}', name:'app_atelier_note')]
    public function noterAtelier(Atelier $atelier, int $value, NoteRepository $noteRepository,
                                 EntityManagerInterface $manager, UserRepository $userRepository): Response
    {
        $user = $userRepository->findOneBy(['email' => $this->getUser()->getUserIdentifier()]);

        if ($user === null) {
            return $this->redirectToRoute('app_login');
        }

        if ($atelier->getListeapprentis()->contains($user) || ($value >= 0 && $value <= 5)) {
            $note = $noteRepository->findOneBy(['user' => $user->getId(), 'atelier' => $atelier->getId()]);

            if ($note === null) {
                $note = new Note();
                $note->setUser($user);
                $note->setAtelier($atelier);
            }

            $note->setValue($value);

            $manager->persist($note);
            $manager->flush();
        }

        return $this->redirectToRoute('app_atelier_inscriptions');
    }

    #[Route('/removenote/{id}', name: 'app_atelier_removenote')]
    public function removeNote(Atelier $atelier, NoteRepository $noteRepository,
                               EntityManagerInterface $manager, UserRepository $userRepository): Response
    {
        $user = $userRepository->findOneBy(['email' => $this->getUser()->getUserIdentifier()]);

        if ($user === null) {
            return $this->redirectToRoute('app_login');
        }

        $note = $noteRepository->findOneBy(['user' => $user->getId(), 'atelier' => $atelier->getId()]);

        if ($note !== null) {
            $manager->remove($note);
            $manager->flush();
        }

        return $this->redirectToRoute('app_atelier_inscriptions');
    }
}
