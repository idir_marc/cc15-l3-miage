<?php

namespace App\DataFixtures;

use App\Entity\Atelier;
use App\Entity\User;
use cebe\markdown\Markdown;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class AtelierFixtures extends Fixture
{
    private UserPasswordHasherInterface $passwordHasher;

    public function __construct(UserPasswordHasherInterface $passwordHasher)
    {
        $this->passwordHasher = $passwordHasher;
    }    public function load(ObjectManager $manager): void
    {
        $faker = \Faker\Factory::create("FR_fr");

        $users=[];
        $roles=[["ROLE_ADMIN","ROLE_INSTRUCTEUR"],["ROLE_APPRENTI"],["ROLE_APPRENTI"],["ROLE_APPRENTI","ROLE_INSTRUCTEUR"],
            ["ROLE_APPRENTI"],["ROLE_APPRENTI"],["ROLE_APPRENTI"],["ROLE_APPRENTI","ROLE_INSTRUCTEUR"]];

        for ($i=0;$i<sizeof($roles);$i++){
            $user = new User();
            $user->setNom($faker->name)->setPrenom($faker->name)->setEmail($faker->email)
                 ->setPassword($this->passwordHasher->hashPassword($user,'123456'));
            $user->setRoles($roles[$i]);
            $users[]=$user;
            $manager->persist($user);
        }

        $listeAtelier=[];

        for ($i = 1; $i <= 5; $i++)
        {
            $atelier = new Atelier();
            $atelier->setName($faker->word)
                    ->setDescription("**<p>" . join("</p><p>", $faker->paragraphs) . "</p>**");
            $atelier->setInstructeur($users[0]);
            $listeAtelier[]=$atelier;
            $manager->persist($atelier);
        }

        foreach($users as $apprenti){
            if(in_array("ROLE_APPRENTI",$apprenti->getRoles())){
                $apprenti->addListeAtelier($listeAtelier[0]);
            }
        }

        $manager->flush();
    }
}
