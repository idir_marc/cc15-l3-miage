<?php

namespace App\Entity;

use App\Repository\AtelierRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: AtelierRepository::class)]
class Atelier
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?string $name = null;

    #[ORM\Column(length: 255)]
    private ?string $description = null;

    #[ORM\ManyToOne]
    private ?User $instructeur = null;

    #[ORM\ManyToMany(targetEntity: User::class, inversedBy: 'listeAteliers')]
    private Collection $listeapprentis;

    #[ORM\OneToMany(mappedBy: 'atelier', targetEntity: Note::class, orphanRemoval: true)]
    private Collection $notes;

    public function __construct()
    {
        $this->listeapprentis = new ArrayCollection();
        $this->notes = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getInstructeur(): ?User
    {
        return $this->instructeur;
    }

    public function setInstructeur(?User $instructeur): self
    {
        $this->instructeur = $instructeur;

        return $this;
    }

    /**
     * @return Collection<int, User>
     */
    public function getListeapprentis(): Collection
    {
        return $this->listeapprentis;
    }

    public function addListeapprenti(User $apprenti): self
    {
        if ($this->instructeur === $apprenti) {
            return $this;
        }

        if (!$this->listeapprentis->contains($apprenti)) {
            $this->listeapprentis->add($apprenti);
            $apprenti->addListeAtelier($this);
        }

        return $this;
    }

    public function removeListeApprenti(User $listeapprenti): self
    {
        $this->listeapprentis->removeElement($listeapprenti);

        return $this;
    }

    /**
     * @return Collection<int, Note>
     */
    public function getNotes(): Collection
    {
        return $this->notes;
    }

    public function addNote(Note $note): self
    {
        if (!$this->notes->contains($note)) {
            $this->notes->add($note);
            $note->setAtelier($this);
        }

        return $this;
    }

    public function removeNote(Note $note): self
    {
        if ($this->notes->removeElement($note)) {
            // set the owning side to null (unless already changed)
            if ($note->getAtelier() === $this) {
                $note->setAtelier(null);
            }
        }

        return $this;
    }

    public function avgNotes(): string
    {
        $s = 0;
        if ($this->notes->count() == 0) return '-';
        foreach ($this->notes as $note) {
            $s += $note->getValue();
        }
        $s = $s/$this->notes->count();
        return ''.round($s, 2);
    }
}
